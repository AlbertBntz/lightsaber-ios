//
//  SHBox2dBodyHolder.h
//  Anti-heroes Wars
//
//  Created by Denis on 3/31/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Box2D.h"

@interface SHBox2dBodyHolder : NSObject
{
    b2Body *_theB2Body;
    b2World *_theWorld;
}
@property (nonatomic,assign) b2Body *theB2Body;
@property (nonatomic,assign) b2World *theWorld;

-(void)purgeStoredData;
@end
