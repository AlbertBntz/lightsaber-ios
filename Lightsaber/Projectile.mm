//
//  Projectile.m
//  SomeShooter
//
//  Created by Denis on 3/16/12.
//  Copyright 2012 __MyCompanyName__. All rights reserved.
//

#import "Projectile.h"
#import "SHProjectileTrail.h"

#define PROJECTILE_B2D_COLLISION_ENABLED
#define TRAIL_ENABLED

@implementation Projectile
@synthesize type = _type, speed = _speed;
@synthesize myInfo = _myInfo;
@synthesize destroyed = _destroyed;
@synthesize sprite = _sprite;
//@synthesize mstreak = _mstreak;
-(id)initWithType:(projectileType)incType andSpeedMod:(float)incSpeedMod withAngle:(float)angle onLayer:(CCLayer *)layer
{
    self = [super init];
    _destroyed = _prevPosDefined = NO;
    _type = incType;
    _move = ccp(0,0);
    // "100" is the speed accroding to type
    _speed = 1 * incSpeedMod/100;
    // define sprite here
    _sprite = [CCSprite spriteWithFile:@"Spheres/Projectiles/orange_laser.png"];
    _initialSpriteWidth = _sprite.boundingBox.size.width+10;
    _sprite.anchorPoint = ccp(0.5f,1.0f);
    [self addChild:_sprite];
    _sprite.opacity = 0;
    _sprite.rotation = CC_RADIANS_TO_DEGREES(-(angle))-90;
    //
    _move = ccpForAngle(angle);
    _move = ccpMult(_move, _speed);
    _previousPosition = CGPointZero;
//    self.mstreak = nil;
    _trail = nil;
    _layer = layer;
//    NSLog(@"Streak gen: %f,%f, self pos: %f,%f",_mstreak.position.x,_mstreak.position.y,self.position.x,self.position.y);
    return self;
}

-(void)onEnter
{
    [super onEnter];
    [self scheduleUpdate];
    _sprite.opacity = 0;
}

-(void)update:(ccTime)dt
{
#ifdef TRAIL_ENABLED
    if (_trail == nil)
    {
//        self.mstreak = [CCMotionStreak streakWithFade:0.2 minSeg:3 width:8 color:ccc3(255, 255, 255) textureFilename:@"Icon.png"];
        _trail = [SHProjectileTrail trailNodeWithMotionStreakWithFade:0.2 minSeg:0.5 width:_initialSpriteWidth textureFilename:@"Spheres/Projectiles/trail.png" onLayer:_layer withPosition:_myInfo.position];
        [_trail retain];
    }
    else {
        [_trail.motionStreak setPosition:_myInfo.position];
    }
#endif
//    NSLog(@"SELF PROJECTILE RETAIN COUNT on update:%i",[self retainCount]);
    if (!_prevPosDefined)
    {
        if (!CGPointEqualToPoint(_previousPosition, CGPointZero))
            _prevPosDefined = YES;
    }
    else
    {
        CGPoint diff = ccpSub(_myInfo.position, _previousPosition);
        float ang = ccpAngleSigned(ccp(0,1), diff);
        _sprite.rotation = CC_RADIANS_TO_DEGREES(-ang);
    }
    
    _previousPosition = _myInfo.position;
    
    CGPoint nextPosition = _myInfo.position;
    if (!_myInfo.impulseApplied)
    {
        _myInfo.b2impulse = _move;
        _myInfo.impulseApplied = YES;
    }
//    NSLog(@"proj impulse is: %f,%f", _myInfo.b2impulse.x,_myInfo.b2impulse.y);
    bool destroyCheck = NO;
    

#ifdef PROJECTILE_B2D_COLLISION_ENABLED
    if (_myInfo.b2dContactDetected)
    {
        _myInfo.bounces--;
        _myInfo.b2dContactDetected = NO;
        if (_myInfo.bounces < 0)
        {
            destroyCheck = YES;
        }
    }

#endif
    
    if (!destroyCheck)
    {
        self.position = nextPosition;
    }
    else
    {
        [self removeAllChildrenWithCleanup:YES];
        [self removeFromParentAndCleanup:YES];
//            NSLog(@"SELF PROJECTILE RETAIN COUNT AFTER DESTROY:%i",[self retainCount]);
        _destroyed = YES;
    }
    
}

-(void)onProjectileContactStateChange:(ProjectileContactState)toState
{
    if (_trail != nil)
    {
        switch (toState) {
            case pcsSaber:
                _trail.motionStreak.color = ccc3(25, 211, 245);
                break;
                
            case pcsEnemy:
                _trail.motionStreak.color = ccc3(227, 32, 12);
                break;

                
            default:
                break;
        }
    }
}

-(void)destroy
{
//    CCLOG(@"Projectile info retain count is: %i",[_myInfo retainCount]);
    [_myInfo release]; // that will call b2body destroy;
    _myInfo = nil;
}

-(void)dealloc
{
//    [self.mstreak removeFromParentAndCleanup:YES];
//    self.mstreak = nil;
    [_trail destroyInTimeUnattached:1.0f];
    [_trail removeFromParentAndCleanup:NO];
    [self destroy];
    [super dealloc];
}
@end
