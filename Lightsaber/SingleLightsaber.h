//
//  SingleLightsaber.h
//  Lightsaber
//
//  Created by Denis on 3/29/12.
//  Copyright 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "cocos2d.h"
#import "SingleLightsaberActorInfo.h"
#import "Box2D.h"
#import "LTargetProtocol.h"

@class LPlume;


@interface SingleLightsaber : CCNode <LTargetProtocol> {
    CCSprite *_beam;
    CCSprite *_handle;
    
    SingleLightsaberActorInfo *_myInfo;
    
    float _radius;
    
    bool _touchEnabled;
    CGPoint _touchLocation;
    CGPoint _handlePoint;
    
    b2MouseJoint *_mouseJoint;
    bool _isDualTouched;
    CCLayer *_layer;
    LPlume *_plume;
    
    CGPoint _currentBeamCenterPosition;
    CGPoint _previousBeamCenterPosition;
    
    CGSize _initialHandleSize;
    b2MassData _savedForDoubletouchMassData;
    float _savedForDoubletouchI;
}
@property (nonatomic, retain) SingleLightsaberActorInfo *myInfo;
@property bool touchEnabled;
@property CGPoint touchLocation;
@property bool isDualTouched;
@property (nonatomic, readonly) CCSprite *beam;
@property (nonatomic, readonly) CCSprite *handle;


-(id)initOnLayer:(CCLayer*)lay;

-(void)setupPhysicsBody;

-(bool)positionIsWithinRadius:(CGPoint)pos;

-(void)onTouchBegin;
-(void)onTouchMoved;
-(void)onTouchCancelled;
-(void)onTouchEnded;

-(CGPoint)getTargetPosition;
@end
