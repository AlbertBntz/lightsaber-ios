//
//  GameActorInfo.mm
//  Shooter
//
//  Created by Denis on 3/25/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "GameActorInfo.h"
#import "SHBox2dBodyHolder.h"

@implementation GameActorInfo
@synthesize hostObject = _hostObject;
@synthesize position = _position;
@synthesize rotation = _rotation;
@synthesize gotType = _gotType;
@synthesize attType = _attType;
@synthesize collisionDetected = _collisionDetected;
@synthesize b2dContactDetected = _b2dContactDetected;
@synthesize b2bvelocity = _b2bvelocity;
@synthesize b2impulse = _b2impulse;
@synthesize forcedPosition = _forcedPosition;
@synthesize forcedReposition = _forcedReposition;
@synthesize b2dBodyHolder = _b2dBodyHolder;

-(id)initWithPosition:(CGPoint)pos andRotation:(CGFloat)rotation andRect:(CGRect)rect andGotType:(gameObjectType)got andAttType:(attachedToType)att
{
    self = [super init];
    // NOTE! The holder is inited, but its body will be nil until something else sets it.
    SHBox2dBodyHolder *holder = [[SHBox2dBodyHolder alloc] init];
    _physicsBodyRemoved = NO;
    _b2dBodyHolder = holder;
    _position = pos;
    _rect = rect;
    _gotType = got;
    _attType = att;
    _collisionDetected = _b2dContactDetected = NO;
    _b2bvelocity = _b2impulse = CGPointZero;
    _forcedReposition = NO;
    _forcedPosition = ccp(0,0);
    return self;
}

-(void)setRect:(CGRect)rect
{
    rect = _rect;
}
-(CGRect)rect
{
    return CGRectMake(_position.x-_rect.size.width/2, _position.y-_rect.size.height/2, _rect.size.width, _rect.size.height);
}

-(void)dealloc
{    
    if (!_physicsBodyRemoved)
    {
        [_b2dBodyHolder purgeStoredData];
        [_b2dBodyHolder release];
    }
    [super dealloc];
}
@end
