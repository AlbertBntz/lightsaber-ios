//
//  SingleLightsaberActorInfo.mm
//  Lightsaber
//
//  Created by Denis on 3/29/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "SingleLightsaberActorInfo.h"
#import "SHb2dCollisionManager.h"
#import "LContactHolder.h"
#import "SimpleAudioEngine.h"

@implementation SingleLightsaberActorInfo
@synthesize angularVelocity = _angularVelocity;
@synthesize beamRect = _beamRect;
@synthesize anchorOfBodyToHandle = _anchorOfBodyToHandle;
@synthesize dragForceMultiplier = _dragForceMultiplier;
@synthesize handleContactsPool = _handleContactsPool;
-(id)initWithPosition:(CGPoint)pos andRotation:(CGFloat)rotation andRect:(CGRect)rect andGotType:(gameObjectType)got andAttType:(attachedToType)att andBeamRect:(CGRect)bRect andHandleRect:(CGRect)handleRect
{
    self = [super initWithPosition:pos andRotation:rotation andRect:rect andGotType:got andAttType:att];
    _beamRect = bRect;
    _handleRect = handleRect;
    _anchorOfBodyToHandle = ccp(0,_handleRect.size.height*0.9/PTM_RATIO);
    _dragForceMultiplier = 1000.0f;
    _handleContactsPool = [NSMutableArray new];
    return self;
}


-(void)pushContactedObjectInfoToHandleContactsPool:(GameActorInfo*)objInfo;
{
    for (LContactHolder *h in _handleContactsPool)
    {
        if ([h.contactedObjectInfo isEqual:objInfo])
        {
            return;
        }
    }
    
    LContactHolder *newHolder = [LContactHolder LCHolderWithContactedObjectInfo:objInfo];
    [_handleContactsPool addObject:newHolder];
//    [[SimpleAudioEngine sharedEngine] playEffect:@"Sound/Pain.mp3"];
}

-(void)dealloc
{
    self.handleContactsPool = nil;
    [super dealloc];
}
@end
