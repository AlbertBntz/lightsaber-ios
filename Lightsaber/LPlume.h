//
//  LPlume.h
//  Lightsaber
//
//  Created by Denis on 4/18/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "cocos2d.h"

struct MotionStreakInitData {
    ccColor3B color;
    float width;
    float fade;
    float minSeg;
    CCTexture2D *texture;
};

@interface LPlume : NSObject
{
    NSMutableArray *_motionStreaks;
    NSMutableArray *_motionStreaksPositions;
    NSMutableArray *_segments;
    CCLayer *_layer;
    CCNode *_saberNode;
    int zInd;
    bool _didReset;
}

-(id)initWithBeamSize:(CGSize)bSize andCenterPoint:(CGPoint)cPoint andSaberNode:(CCNode*)sNode andMotionStreakInitData:(struct MotionStreakInitData)initData onLayer:(CCLayer*)layer withZIndex:(int)zed;
-(void)updateStreaks;
-(void)resetStreaks;
-(void)interruptStreaks;
@end
