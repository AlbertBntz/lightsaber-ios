//
//  GameplayLayer.h
//  SomeShooter
//
//  Created by Denis on 3/16/12.
//  Copyright __MyCompanyName__ 2012. All rights reserved.
//



#import "cocos2d.h"
#import "SingleLightsaber.h"

@class SHb2dCollisionManager;
@class HUDLayer;

@interface GameplayLayer  : CCLayer 
{
    NSMutableArray *_projectiles;
    
    float _timeAccum;
    
    bool _slowMoToggle;
    float _slowMoTimeAccum;
    float _slowMoTimeScaleTargetValue;
    float _slowMoTimeScaleBeginValue;

    SHb2dCollisionManager *_b2dColMan;
    
    SingleLightsaber *_saber;
    
    HUDLayer *_HUD;
    
    UITouch *_firstTouchOnSaber;
    UITouch *_secondTouchOnSaber;
}
@property (nonatomic, retain) NSMutableArray *projectiles;
@property bool isOnSlowMo;
@property (nonatomic, assign) HUDLayer *HUD;
@property (nonatomic, readonly) UITouch *firstTouchOnSaber;
@property (nonatomic, readonly) UITouch *secondTouchOnSaber;
+(CCScene*)scene;

-(void)addSaber;

-(void)update:(ccTime)dt;

-(void)doSlowMo;
-(void)undoSlowMo;


-(void)b2dCollisionSimulation:(ccTime)dt;

-(void)testAddSphere;

@end
