//
//  LContactHolder.m
//  Lightsaber
//
//  Created by Denis on 5/4/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "LContactHolder.h"

@implementation LContactHolder
@synthesize contactedObjectInfo = _contactedObjectInfo;
@synthesize lifeTimer = _lifeTimer;
-(id)init
{
    self = [super init];
    _lifeTimer = 0;
    _contactedObjectInfo = nil;
    return self;
}

+(LContactHolder*)LCHolderWithContactedObjectInfo:(GameActorInfo *)objInfo
{
    LContactHolder *holder = [[[LContactHolder alloc] init] autorelease];
    holder.contactedObjectInfo = objInfo;
    holder.lifeTimer = 2;
    return holder;
}

-(void)dealloc
{
    self.contactedObjectInfo = nil;
    [super dealloc];
}
@end
