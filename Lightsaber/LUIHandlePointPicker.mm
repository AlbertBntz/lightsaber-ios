//
//  LUIHandlePointPicker.mm
//  Lightsaber
//
//  Created by Denis on 4/13/12.
//  Copyright 2012 __MyCompanyName__. All rights reserved.
//

#import "LUIHandlePointPicker.h"
#import "SingleLightsaberActorInfo.h"
#import "SHb2dCollisionManager.h"

@implementation LUIHandlePointPicker
@synthesize state = _state;
@synthesize labelState = _labelState;
@synthesize backgroundRect = _backgroundRect;
-(id)initWithHandleSprite:(CCSprite *)handle andBeamSprite:(CCSprite *)beam andInfo:(SingleLightsaberActorInfo *)info
{
    self = [super init];
    _touchTookPlace = NO;
    _handle = [CCSprite spriteWithTexture:handle.texture];
    _beam = [CCSprite spriteWithTexture:beam.texture];
    _bg = [CCSprite spriteWithFile:@"Saber/HandlePointPicker/back.png"];
//    _bg.scaleX = 2.4f;
//    _bg.scaleY = 2.4f;
    _picker = [CCSprite spriteWithFile:@"Saber/HandlePointPicker/picker.png"];
    _picker.opacity = 0;
    _info = info;
    _state = LUIHPPClosed;
    _labelState = HPPLSHidden;
    
    _conversionCoeff = 0.5f;
    _handle.anchorPoint = _beam.anchorPoint = ccp(0.5f,0.0f);
    _bg.anchorPoint = ccp(0.0f,0.5f);
    _bg.rotation = -90;
    [self addChild:_bg z:0];
    
    _label = [CCSprite spriteWithFile:@"Saber/HandlePointPicker/picker.png"];
    _label.rotation = 90;
    _label.position = ccp(_bg.position.x+_bg.boundingBox.size.width/2-_label.boundingBox.size.width,_bg.position.y+_bg.boundingBox.size.height/2);
    [self addChild:_label z:1];
    _handle.position = ccp(0,LUIHPP_SABER_OFFSET_IN_PERCENT_OF_BG*_bg.boundingBox.size.height);
    _beam.position = ccp(0,_handle.position.y+_handle.boundingBox.size.height);
    [self addChild:_handle z:2];
    [self addChild:_beam z:3];
    _sliderStartPoint = ccp(0,_handle.position.y);
    _sliderEndPoint = ccp(0,_beam.position.y+_beam.boundingBox.size.height);
    _sliderLenght = fabsf(_sliderEndPoint.y-_sliderStartPoint.y);
//    NSLog(@"K%f",_sliderLenght);
    [self addChild:_picker z:4];
    
    // rescaling bg to fit content
    float bgW = _bg.boundingBox.size.width;
    float bgH = _bg.boundingBox.size.height;
    float cW = _handle.boundingBox.size.width+_handle.boundingBox.size.width*2.5f; // additional % for width
    float cH = _handle.boundingBox.size.height+_beam.boundingBox.size.height + _handle.position.y*2; // additional offset for height (LUIHPP_SABER_OFFSET_IN_PERCENT_OF_BG x2)
    
    float epsilon = 0.01f;
    
    float widthSizeCoeff = 1.0f;
    float heightSizeCoeff = 1.0f;
    float bgTestW = bgW;
    float bgTestH = bgH;
    while (bgTestW<cW) {
        bgTestW = bgW * widthSizeCoeff;
        widthSizeCoeff+=epsilon;
    }
    while (bgTestH<cH) {
        bgTestH =bgH * heightSizeCoeff;
        heightSizeCoeff+=epsilon;
    }
    _bg.scaleX = heightSizeCoeff;
    _bg.scaleY = widthSizeCoeff;
    
    _backgroundRect = _bg.boundingBox;
    
    // repeat label reposition
    _label.position = ccp(_bg.position.x+_bg.boundingBox.size.width/2-_label.boundingBox.size.width/2,_bg.position.y+_bg.boundingBox.size.height/2);
    return self;
}

-(void)onEnter
{
    [super onEnter];
    [self scheduleUpdate];
}

-(void)update:(ccTime)dt
{
    _picker.position = CGPointMake(0, _sliderStartPoint.y+_info.anchorOfBodyToHandle.y*PTM_RATIO);
    if (self.state == LUIHPPOpened)
    {
        _label.position = ccp(_label.position.x,_picker.position.y);
    }
    
    if (self.state == LUIHPPClosed)
    {
        
    }
    else if (self.state == LUIHPPOpened) {
        
    }
    else if (self.state == LUIHPPAnimation) {
        
    }
}

-(bool)locationWithinNode:(CGPoint)pos
{
    return CGRectContainsPoint(_bg.boundingBox, pos);
}

-(bool)locationWithinLabel:(CGPoint)pos
{
    return CGRectContainsPoint(_label.boundingBox, pos);
}

-(bool)locationNearCrossingBorder:(CGPoint)location
{
    //location in world, not in node
    return ((location.x>-1 && location.x<15) && (location.y>self.position.y));
}

-(bool)onTouchBegin:(CGPoint)location
{
    bool result = NO; //result = swallow touch
    CGPoint pos = [self convertToNodeSpace:location];
    if ([self locationNearCrossingBorder:location] && ![self locationWithinLabel:pos])
    {
        result = YES;
        if (self.labelState == HPPLSHidden && self.labelState!=HPPLSAnimation && self.state != LUIHPPAnimation && self.state != LUIHPPOpened)
        {
            [self animateLabelShow];
        }
        else if (self.labelState == HPPLSShown && self.labelState!=HPPLSAnimation && self.state != LUIHPPAnimation && self.state != LUIHPPOpened)
        {
            [self animateLabelHide];
        }
    }
    else if ([self locationWithinLabel:pos] && self.state == LUIHPPClosed && self.labelState == HPPLSShown)
    {
        [self animateSwitchOn];
        result = YES;
        
    }
    else if (self.state == LUIHPPOpened && ![self locationWithinNode:pos]) {
        [self animateSwitchOff];
    }
    else if (self.state == LUIHPPOpened && [self locationWithinNode:pos]) {
        result = YES;
        float y = pos.y;
        if (y>_sliderStartPoint.y && y<_sliderEndPoint.y)
        {
            float diffY = (y - _sliderStartPoint.y);
            float newAnchorY = diffY/PTM_RATIO;
            _info.anchorOfBodyToHandle = ccp(_info.anchorOfBodyToHandle.x, newAnchorY);
            _touchTookPlace = YES;
        }
    }
    return result;
}

-(bool)onTouchMoved:(CGPoint)location
{
    bool result = NO;//result = swallow touch
    CGPoint pos = [self convertToNodeSpace:location];
    if ([self locationWithinLabel:pos] && self.state == LUIHPPClosed && self.labelState == HPPLSShown)
    {
        result = YES;
        [self animateSwitchOn];
    }
    else if ([self locationWithinNode:pos] && self.state == LUIHPPOpened)
    {
        result = YES;
//            _picker.position = CGPointMake(0, _sliderStartPoint.y+_sliderLenght*_info.anchorOfBodyToHandle.y+_handle.boundingBox.size.height/2);
        float y = pos.y;
        if (y>_sliderStartPoint.y && y<_sliderEndPoint.y)
        {
            float diffY = (y - _sliderStartPoint.y);
            float newAnchorY = diffY/PTM_RATIO;
            _info.anchorOfBodyToHandle = ccp(_info.anchorOfBodyToHandle.x, newAnchorY);
            _touchTookPlace = YES;
        }
    }
    else if (self.state == LUIHPPOpened) {
        [self animateSwitchOff];
    }
    return result;
}

-(bool)onTouchEnded:(CGPoint)location
{
    bool result = NO;//result = swallow touch
    CGPoint pos = [self convertToNodeSpace:location];
    if ([self locationWithinNode:pos])
    {
        result = YES;
    }
    else if (self.state == LUIHPPOpened) {
        [self animateSwitchOff];
    }
    return result;
}

-(void)switchOn
{
    self.state = LUIHPPOpened;
}

-(void)switchOff
{
    self.state = LUIHPPClosed;
    _touchTookPlace = NO;
}

-(void)animateSwitchOn
{
    self.state = LUIHPPAnimation;
    [self stopActionByTag:LUIHPP_MOVE_ACTION_TAG];
    CCMoveBy *m = [CCMoveBy actionWithDuration:0.5f position:ccp(_bg.boundingBox.size.width,0)];
    m.tag = LUIHPP_MOVE_ACTION_TAG;
    CCCallFunc *c = [CCCallFunc actionWithTarget:self selector:@selector(switchOn)];
    
    CCRotateBy *rl = [CCRotateBy actionWithDuration:0.4f angle:-180];
//    CCScaleBy *rl = [CCScaleBy actionWithDuration:0.5f scaleX:1 scaleY:-1];
    rl.tag = LUIHPP_ROTATE_LBL_ACTION_TAG;
    [_label runAction:rl];
    CCDelayTime *delay = [CCDelayTime actionWithDuration:5.0f];
    CCCallFunc *delayedSwitchOff = [CCCallFunc actionWithTarget:self selector:@selector(delayedSwitchOff)];
    CCSequence *seq = [CCSequence actions:m, c, delay, delayedSwitchOff,nil];
    [self runAction:seq];
}

-(void)animateSwitchOff
{
    self.state = LUIHPPAnimation;
    [self stopActionByTag:LUIHPP_MOVE_ACTION_TAG]; 
    [self stopActionByTag:LUIHPP_ROTATE_LBL_ACTION_TAG];
    CCMoveBy *m = [CCMoveBy actionWithDuration:0.5f position:ccp(-_bg.boundingBox.size.width,0)];
    m.tag = LUIHPP_MOVE_ACTION_TAG;
    CCCallFunc *c = [CCCallFunc actionWithTarget:self selector:@selector(switchOff)];
    CCRotateBy *rl = [CCRotateBy actionWithDuration:0.4f angle:180];
//    CCScaleBy *rl = [CCScaleBy actionWithDuration:0.5f scaleX:1 scaleY:-1];
    rl.tag = LUIHPP_ROTATE_LBL_ACTION_TAG;
    [_label runAction:rl];
    CCDelayTime *delay = [CCDelayTime actionWithDuration:5.0f];
    CCCallFunc *delayedLabelHide = [CCCallFunc actionWithTarget:self selector:@selector(delayedLabelHide)];
    CCSequence *seq = [CCSequence actions:m, c, delay, delayedLabelHide, nil];
    [self runAction:seq];
}

-(void)animateLabelShow{
    if (self.state != LUIHPPAnimation)
    {
        self.labelState = HPPLSAnimation;
        [self stopActionByTag:LUIHPP_MOVE_LBL_ACTION_TAG];
        CCMoveBy *ml = [CCMoveBy actionWithDuration:0.15f position:ccp(_label.boundingBox.size.width,0)];
        ml.tag = LUIHPP_MOVE_LBL_ACTION_TAG;
        CCCallBlock *bl = [CCCallBlock actionWithBlock:^(void){ self.labelState = HPPLSShown; }];
        CCSequence *seq = [CCSequence actions:ml,bl, nil];
        [_label runAction:seq];
    }
}

-(void)animateLabelHide{
    if (self.state != LUIHPPAnimation)
    {
        self.labelState = HPPLSAnimation;
        [self stopActionByTag:LUIHPP_MOVE_LBL_ACTION_TAG];
        CCMoveBy *ml = [CCMoveBy actionWithDuration:0.15f position:ccp(-_label.boundingBox.size.width,0)];
        ml.tag = LUIHPP_MOVE_LBL_ACTION_TAG;
        CCCallBlock *bl = [CCCallBlock actionWithBlock:^(void){ self.labelState = HPPLSHidden; }];
        CCSequence *seq = [CCSequence actions:ml,bl, nil];
        [_label runAction:seq];
    }
}

-(void)delayedLabelHide {
    if (self.state == LUIHPPClosed && self.labelState == HPPLSShown)
    {
        self.labelState = HPPLSAnimation;
        [self stopActionByTag:LUIHPP_MOVE_LBL_ACTION_TAG];
        CCMoveBy *ml = [CCMoveBy actionWithDuration:0.15f position:ccp(-_label.boundingBox.size.width,0)];
        ml.tag = LUIHPP_MOVE_LBL_ACTION_TAG;
        CCCallBlock *bl = [CCCallBlock actionWithBlock:^(void){ self.labelState = HPPLSHidden; }];
        CCSequence *seq = [CCSequence actions:ml,bl, nil];
        [_label runAction:seq];
    }
}

-(void)delayedSwitchOff {
    if ( !_touchTookPlace && self.state != LUIHPPAnimation && self.state == LUIHPPOpened)
    {
        [self animateSwitchOff];
    }
}
@end
