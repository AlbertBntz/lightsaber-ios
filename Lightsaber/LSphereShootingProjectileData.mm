//
//  LSphereShootingProjectileData.mm
//  Lightsaber
//
//  Created by Denis on 4/22/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "LSphereShootingProjectileData.h"

@implementation LSphereShootingProjectileData
@synthesize angle;
@synthesize delay;
@synthesize speedMod;
@synthesize type;
@synthesize damage;
@synthesize firePortNum;
-(id)init
{
    self = [super init];
    return self;
}

@end
