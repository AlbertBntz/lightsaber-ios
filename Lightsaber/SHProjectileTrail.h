//
//  SHProjectileTrail.h
//  Anti-heroes Wars
//
//  Created by Denis on 4/13/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "cocos2d.h"


@interface SHProjectileTrail : CCNode
{
    CCMotionStreak *_motionStreak;
    bool _destroyTimerToggle;
    float _timeAccum;
    float _destroyTime;
}
@property (nonatomic, retain) CCMotionStreak *motionStreak;
+(id)trailNodeWithMotionStreakWithFade:(float)f minSeg:(float)m width:(float)w textureFilename:(NSString *)t onLayer:(CCLayer*)l withPosition:(CGPoint)p; 
-(void)destroyInTimeUnattached:(float)t;

@end
