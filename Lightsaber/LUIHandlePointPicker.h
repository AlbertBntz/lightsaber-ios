//
//  LUIHandlePointPicker.h
//  Lightsaber
//
//  Created by Denis on 4/13/12.
//  Copyright 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "cocos2d.h"

@class SingleLightsaberActorInfo;

#define LUIHPP_SABER_OFFSET_IN_PERCENT_OF_BG .05f

#define LUIHPP_MOVE_ACTION_TAG 192
#define LUIHPP_ROTATE_LBL_ACTION_TAG 193
#define LUIHPP_MOVE_LBL_ACTION_TAG 194

typedef enum {LUIHPPClosed,LUIHPPOpened, LUIHPPAnimation}LUIHandlePointPickerState;
typedef enum {HPPLSShown,HPPLSHidden,HPPLSAnimation}HPPLabelState;

@interface LUIHandlePointPicker : CCNode {
    LUIHandlePointPickerState _state;
    HPPLabelState _labelState;
    CCSprite *_bg;
    CCSprite *_picker;
    CCSprite *_label;
    CCSprite *_handle;
    CCSprite *_beam;
    SingleLightsaberActorInfo *_info;
    CGPoint _sliderStartPoint;
    CGPoint _sliderEndPoint;
    float _sliderLenght;
    float _conversionCoeff;
    CGRect _backgroundRect;
    bool _touchTookPlace;
}
@property LUIHandlePointPickerState state;
@property HPPLabelState labelState;
@property CGRect backgroundRect;
-(bool)onTouchBegin:(CGPoint)location;
-(bool)onTouchMoved:(CGPoint)location;
-(bool)onTouchEnded:(CGPoint)location;
-(id)initWithHandleSprite:(CCSprite*)handle andBeamSprite:(CCSprite*)beam andInfo:(SingleLightsaberActorInfo*)info;
-(bool)locationWithinNode:(CGPoint)location;
-(void)switchOn;
-(void)switchOff;
-(void)animateSwitchOn;
-(void)animateSwitchOff;
@end
