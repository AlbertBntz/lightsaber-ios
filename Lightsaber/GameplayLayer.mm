//
//  HelloWorldLayer.mm
//  SomeShooter
//
//  Created by Denis on 3/16/12.
//  Copyright __MyCompanyName__ 2012. All rights reserved.
//


// Import the interfaces
#import "GameplayLayer.h"
#import "SHb2dCollisionManager.h"
#import "HUDLayer.h"
#import "Background.h"
#import "SimpleAudioEngine.h"

//test
#import "LSphere.h"

// HelloWorldLayer implementation
@implementation GameplayLayer
@synthesize projectiles=_projectiles, isOnSlowMo;
@synthesize HUD = _HUD;
@synthesize firstTouchOnSaber = _firstTouchOnSaber;
@synthesize secondTouchOnSaber = _secondTouchOnSaber;

+(CCScene *) scene
{
    //CGSize winSize = [CCDirector sharedDirector].winSize;
    
	CCScene *scene = [CCScene node];

	GameplayLayer *layer = [GameplayLayer node];
	
    HUDLayer *hud = [HUDLayer node];
    layer.HUD = hud;
    hud.gameplay = layer;
    
    Background *bg = [Background node];
    
	[scene addChild: layer z:2];
	[scene addChild: hud z:10];
    [scene addChild: bg z:0];
    
    
    [[SimpleAudioEngine sharedEngine] preloadEffect:@"Sound/Pain.mp3"];
	// return the scene
	return scene;
}

// on "init" you need to initialize your instance
-(id) init
{
	if( (self=[super init])) {
        self.isTouchEnabled = YES;
        
        // zeroing values
        self.isOnSlowMo = NO;
        _slowMoToggle = NO;
        _slowMoTimeScaleBeginValue = 1;
        _slowMoTimeScaleTargetValue = 0.2f;
        _secondTouchOnSaber = _firstTouchOnSaber = nil;
        //
        
        
        // Tuning new box2d collision manager
        _b2dColMan = [SHb2dCollisionManager sharedSHb2dCollisionManager];
        
        
        [self scheduleUpdate];
        
	}
	return self;
}



-(void)onEnter
{
    [super onEnter];
    [self schedule:@selector(b2dCollisionSimulation:) interval:1/20];
    self.scale = 1.0f;
    [self addSaber];
    [self testAddSphere];
}

-(void)addSaber
{
    CGSize winSize = [CCDirector sharedDirector].winSize;
    _saber = [[[SingleLightsaber alloc] initOnLayer:self] autorelease];
    _saber.position = ccp(winSize.width/2,winSize.height/2);
    [self addChild:_saber];
    [self.HUD addHandlePointPickerWithHandleSprite:_saber.handle andBeamSprite:_saber.beam andInfo:_saber.myInfo];
}

-(void)testAddSphere
{
    
    CGSize winSize = [CCDirector sharedDirector].winSize;
    LSphere *sp = [[[LSphere alloc] initWithType:LSTSingle andTarget:_saber] autorelease];
    sp.position = ccp(winSize.width*0.8,winSize.height*0.8);
    [self addChild:sp];
}

-(void)b2dCollisionSimulation:(ccTime)dt
{
    [_b2dColMan tick:dt];
}

-(void)draw
{
    [super draw];
    [_b2dColMan drawDebug];
}


-(void)update:(ccTime)dt;
{
    // check if we need to change timeScale
    if (_slowMoToggle)
    {
        _slowMoTimeAccum+=dt;
        if (_slowMoTimeAccum > 0.025)
        {
            float currentTimeScale = [self scheduler].timeScale;
            if (_slowMoTimeScaleTargetValue < _slowMoTimeScaleBeginValue) // so we are slowing down the motion
            {
                [self scheduler].timeScale -= (_slowMoTimeScaleBeginValue - currentTimeScale)/3+0.01;
                if ([self scheduler].timeScale <= _slowMoTimeScaleTargetValue)
                {
                    // we finished slowing
                    [self scheduler].timeScale = _slowMoTimeScaleTargetValue;
                    _slowMoToggle = NO;
                }
            }
            else if (_slowMoTimeScaleTargetValue > _slowMoTimeScaleBeginValue) // so we are speeding up the motion
            {
                [self scheduler].timeScale += (currentTimeScale - _slowMoTimeScaleBeginValue)/3+0.02;
                if ([self scheduler].timeScale >= _slowMoTimeScaleTargetValue)
                {
                    // we finished speeding
                    [self scheduler].timeScale = _slowMoTimeScaleTargetValue;
                    _slowMoToggle = NO;
                }
            }
            _slowMoTimeAccum = 0;
        }
    }
    else
    {
        _slowMoTimeAccum = 0;
    }
}

-(void)ccTouchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    for (UITouch *touch in touches)
    {
        CGPoint location = [self convertTouchToNodeSpace:touch];
        if (_saber.touchEnabled && [_saber positionIsWithinRadius:location])
        {
            if (_firstTouchOnSaber == nil)
            {
                _firstTouchOnSaber = touch;
            }
            else if (_secondTouchOnSaber == nil)
            {
                _secondTouchOnSaber = touch;
            }
        }
    }
    if (_firstTouchOnSaber != nil)
    {
        _saber.touchLocation = [self convertTouchToNodeSpace:_firstTouchOnSaber];
        [_saber onTouchBegin];
    }
    if (_secondTouchOnSaber != nil)
    {
        _saber.isDualTouched = YES;
    }
    
}

-(void)ccTouchesMoved:(NSSet *)touches withEvent:(UIEvent *)event
{
    bool firstTouchMoved = NO;
    bool secondTouchMoved = NO;
    for (UITouch *touch in touches)
    {
//        CGPoint location = [self convertTouchToNodeSpace:touch];
        if (_saber.touchEnabled)
        {
            if (_firstTouchOnSaber == touch)
            {
                firstTouchMoved = YES;
            }
            else if (_secondTouchOnSaber == touch)
            {
                secondTouchMoved = YES;
            }
        }
    }
    if (_firstTouchOnSaber != nil && firstTouchMoved)
    {
        _saber.touchLocation = [self convertTouchToNodeSpace:_firstTouchOnSaber];
        [_saber onTouchMoved];
    }
}

-(void)ccTouchesCancelled:(NSSet *)touches withEvent:(UIEvent *)event
{
    bool firstTouchCancelled = NO;
    bool secondTouchCancelled = NO;
    for (UITouch *touch in touches)
    {
        //        CGPoint location = [self convertTouchToNodeSpace:touch];
        if (_saber.touchEnabled)
        {
            if (_firstTouchOnSaber == touch)
            {
                firstTouchCancelled = YES;
            }
            else if (_secondTouchOnSaber == touch)
            {
                secondTouchCancelled = YES;
            }
        }
    }
    if (_firstTouchOnSaber != nil && firstTouchCancelled)
    {
        _saber.touchLocation = [self convertTouchToNodeSpace:_firstTouchOnSaber];
        [_saber onTouchCancelled];
        _firstTouchOnSaber = nil;
    }
    if (_secondTouchOnSaber != nil && secondTouchCancelled)
    {
        _saber.isDualTouched = NO;
        _secondTouchOnSaber = nil;
    }
    
}

-(void)ccTouchesEnded:(NSSet *)touches withEvent:(UIEvent *)event
{
    bool firstTouchEnded = NO;
    bool secondTouchEnded = NO;
    for (UITouch *touch in touches)
    {
        //        CGPoint location = [self convertTouchToNodeSpace:touch];
        if (_saber.touchEnabled)
        {
            if (_firstTouchOnSaber == touch)
            {
                firstTouchEnded = YES;
            }
            else if (_secondTouchOnSaber == touch)
            {
                secondTouchEnded = YES;
            }
        }
    }
    if (_firstTouchOnSaber != nil && firstTouchEnded)
    {
        _saber.touchLocation = [self convertTouchToNodeSpace:_firstTouchOnSaber];
        [_saber onTouchEnded];
        _firstTouchOnSaber = nil;
    }
    if (_secondTouchOnSaber != nil && secondTouchEnded)
    {
        _saber.isDualTouched = NO;
        _secondTouchOnSaber = nil;
    }

}


-(void)doSlowMo
{
    _slowMoTimeScaleTargetValue = 0.2f;
    _slowMoTimeScaleBeginValue = [self scheduler].timeScale;
    self.isOnSlowMo = YES;
    _slowMoToggle = YES;
    
}

-(void)undoSlowMo
{
    _slowMoTimeScaleTargetValue = 1.0f;
    _slowMoTimeScaleBeginValue = [self scheduler].timeScale;
    _slowMoToggle = YES;
}



// on "dealloc" you need to release all your retained objects
- (void) dealloc
{
    self.HUD = nil;
    
	// in case you have something to dealloc, do it in this method
	// in this particular example nothing needs to be released.
	// cocos2d will automatically release all the children (Label)
	
	// don't forget to call "super dealloc"
	[super dealloc];
}
@end
