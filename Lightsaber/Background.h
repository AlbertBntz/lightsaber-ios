//
//  Background.h
//  Lightsaber
//
//  Created by Denis on 4/14/12.
//  Copyright 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "cocos2d.h"

@interface Background : CCLayer {
    CCSprite *_bg; 
}
@property (nonatomic, retain) CCSprite *bg;

@end
